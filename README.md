3dsmicrosd
===============

This is a script for the use of the New 3DS' microSD management on Linux.

I primarily develop this script on Gitlab at [https://gitlab.com/JohnoKing/3dsmicrosd](https://gitlab.com/JohnoKing/3dsmicrosd).

## Dependencies
This script depends on cifs-utils and sudo.

## License
This is under the MIT License. You can find it in the LICENSE file.
